package com.example.rizqi.uasandroid.Api;

import com.example.rizqi.uasandroid.Model.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

import static android.R.attr.id;

public interface RestApi {
    @FormUrlEncoded
    @POST("insert.php")
    Call<ResponseModel> sendBiodata(@Field("nama") String nama,
                                    @Field("jenis") String jenis,
                                    @Field("jumlah") String jumlah,
                                    @Field("harga") String harga,
                                    @Field("diskon") String diskon);


    @GET("read.php")
    Call<ResponseModel> getBiodata();
    //update menggunakan 3 parameter
    @FormUrlEncoded
    @POST("update.php")
    Call<ResponseModel> updateData( @Field("id") String id,
                                    @Field("nama") String nama,
                                    @Field("jenis") String jenis,
                                    @Field("jumlah") String jumlah,
                                    @Field("harga") String harga,
                                    @Field("diskon") String diskon);


    @FormUrlEncoded
    @POST("delete.php")
    Call<ResponseModel> deleteData(@Field("id") String id);
}
