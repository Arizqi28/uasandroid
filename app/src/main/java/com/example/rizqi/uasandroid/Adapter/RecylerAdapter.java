package com.example.rizqi.uasandroid.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import com.example.rizqi.uasandroid.MainActivity;
import com.example.rizqi.uasandroid.R;
import com.example.rizqi.uasandroid.Model.DataModel;

/**
 * Created by arizqi on 21-Feb-18.
 */

public class RecylerAdapter extends
        RecyclerView.Adapter<RecylerAdapter.MyHolder> {List<DataModel> mList ;
    Context ctx;
    public RecylerAdapter(Context ctx, List<DataModel> mList) {
        this.mList = mList;
        this.ctx = ctx;
    }
    @Override
    public RecylerAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutlist, parent, false);
        MyHolder holder = new MyHolder(layout);
        return holder;
    }
    @Override
    public void onBindViewHolder(RecylerAdapter.MyHolder holder,
                                 final int position) {
        holder.nama.setText(mList.get(position).getnama());
        holder.jenis.setText(mList.get(position).getjenis());
        holder.jumlah.setText(mList.get(position).getjumlah());
        holder.harga.setText(mList.get(position).getharga());
        holder.diskon.setText(mList.get(position).getdiskon());
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {@Override
        public void onClick(View view) {
            Intent goInput = new Intent(ctx,MainActivity.class);
            try {
                goInput.putExtra("id", mList.get(position).getId());
                goInput.putExtra("nama", mList.get(position).getnama());
                goInput.putExtra("jenis", mList.get(position).getjenis());
                goInput.putExtra("jumlah", mList.get(position).getjumlah());
                goInput.putExtra("harga", mList.get(position).getharga());
                goInput.putExtra("diskon", mList.get(position).getdiskon());

                ctx.startActivity(goInput);
            }catch (Exception e){
                e.printStackTrace();
                Toast.makeText(ctx, "Error data " +e, Toast.LENGTH_SHORT).show();
            }
        }
        });
    }
    @Override
    public int getItemCount()
    {
        return mList.size();
    }
    public class MyHolder extends RecyclerView.ViewHolder {
        TextView nama,jenis,jumlah,harga,diskon;
        DataModel dataModel;
        public MyHolder(View v)
        {
            super(v);
            nama = (TextView) v.findViewById(R.id.tvnama);
            jenis = (TextView) v.findViewById(R.id.tvjenis);
            jumlah = (TextView) v.findViewById(R.id.tvjumlah);
            harga = (TextView) v.findViewById(R.id.tvharga);
            diskon = (TextView) v.findViewById(R.id.tvdiskon);

        }
    }
}