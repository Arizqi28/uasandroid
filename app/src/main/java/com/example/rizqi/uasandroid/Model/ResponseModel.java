package com.example.rizqi.uasandroid.Model;

import com.example.rizqi.uasandroid.Model.DataModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;
@SuppressWarnings("unused")
public class ResponseModel {
    List<DataModel> result;
    @SerializedName("id")
    private String mId;
    @SerializedName("pesan")
    private String mPesan;
    public List<DataModel> getResult() {
        return result;
    }
    public void setResult(List<DataModel> result) {
        this.result = result;
    }
    public String getKode() {
        return mId;
    }
    public void setKode(String kode) {
        mId = kode;
    }
    public String getPesan() {
        return mPesan;
    }
    public void setPesan(String pesan) {
        mPesan = pesan;
    }
}
