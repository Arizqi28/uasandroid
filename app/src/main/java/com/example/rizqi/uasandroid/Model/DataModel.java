package com.example.rizqi.uasandroid.Model;

import com.google.gson.annotations.SerializedName;

public class DataModel {

    @SerializedName("id")
    private String mId;
    @SerializedName("nama")
    private String mnama;
    @SerializedName("jenis")
    private String mjenis;
    @SerializedName("jumlah")
    private String mjumlah;
    @SerializedName("harga")
    private String mharga;
    @SerializedName("diskon")
    private String mdiskon;


    public String getId() {
        return mId;
    }
    public void setId(String id) {
        mId = id;
    }
    public String getnama() {
        return mnama;
    }
    public void setnama(String nama) {
        mnama = nama;
    }
    public String getjenis() {
        return mjenis;
    }
    public void setjenis(String jenis) {
        mjenis = jenis;
    }
    public String getjumlah() {
        return mjumlah;
    }
    public void setjumlah(String jumlah) {
        mjumlah = jumlah;
    }
    public String getharga() {
        return mharga;
    }
    public void setharga(String harga) {mharga = harga;}
    public String getdiskon() {
        return mdiskon;
    }
    public void setdiskon(String diskon) {
        mdiskon = diskon;
    }

}
